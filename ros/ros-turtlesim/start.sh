#!/bin/bash
source /opt/ros/indigo/setup.bash

roslaunch rosbridge_server rosbridge_websocket.launch &
sleep 3
rosrun uvc_camera uvc_camera_node &
rosrun turtlesim turtlesim_node &
rosrun web_video_server web_video_server _port:=9696 &
bash