Build image
-----

    docker build -t turtlesim .

Run image
-----
    xhost + # allows docker to use local XWindow Server

    docker run \
        -it \
        --net host \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        --device=/dev/video0 \
        -e DISPLAY=unix$DISPLAY \
        -v /dev/shm:/dev/shm \
        turtlesim

Exposed:
-----------
Camera stream is exposed at http://localhost:9696/stream_viewer?topic=/image_raw
Websocket to rosbridge on port 9090
ROS Master on port 11311
