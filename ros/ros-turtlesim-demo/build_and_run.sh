#!/bin/bash
docker build -t turtlesim .

xhost + # allows docker to use local XWindow Server

docker run \
    -it \
    --net host \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    --device=/dev/video0 \
    -e DISPLAY=unix$DISPLAY \
    -v /dev/shm:/dev/shm \
    turtlesim