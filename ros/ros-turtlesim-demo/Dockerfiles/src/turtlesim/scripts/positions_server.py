#!/usr/bin/env python
import rospy
from turtlesim.msg import Pose
from turtlesim.srv import Spawn

import sys
import socket
from threading import Thread


TCP_IP = '0.0.0.0'
TCP_PORT = 5005


def get_name(pos_topic):
    return pos_topic.split("/")[1]

class PositionsServer:
    def __init__(self):
        rospy.wait_for_service('/spawn')
        self.spawn = rospy.ServiceProxy('/spawn', Spawn)

        #self.spawn(3,4,1.57,"s1","sensorsmall.png")
        #self.spawn(3,8,1.57,"s2","sensorsmall.png")
        #self.spawn(4,2,1.57,"h1","huskysmall.png")
        #self.spawn(1,1,1.57,"b1","basesmall.png")

        self.acquire()


        try:
            self.run_server()
        except Exception,e:
            try:
                self.server_socket.close()
            except:
                pass
            print "Unhandled exception",e
            sys.exit(1)
        except (KeyboardInterrupt, SystemExit):
            try:
                self.server_socket.close()
            except:
                pass
            sys.exit(0)



    def acquire(self):
        print "Acquire"
        s = ""
        topics = rospy.get_published_topics()
        for top in topics:
            if top[0].endswith("/pose"):
                #print top
                t = get_name(top[0])
                msg = rospy.wait_for_message(top[0],Pose)
                #print top[0],msg.x,msg.y
                s += str(t)+" "+str(msg.x)+" "+str(msg.y)+"\n"
        print s
        return s+"\n"

    def run_server(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.server_socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_QUICKACK, 1)
        self.server_socket.setblocking(1)

        self.server_socket.bind((TCP_IP, TCP_PORT))
        self.server_socket.listen(10)

        self.is_running = True
        while self.is_running:
            print "Waiting for connection"              
            conn, addr = self.server_socket.accept()
            conn.setblocking(1)
            conn.setsockopt( socket.IPPROTO_TCP, socket.TCP_NODELAY, 1 )
            conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_QUICKACK, 1)

            print "Connection address:", addr
            th = Thread(target=lambda: self.run_handler(conn,addr))
            th.setDaemon(True)

            th.start()
        self.server_socket.close()
        print "Closed server\n"


    def run_handler(self,conn,addr):
        conn.send(self.acquire())
        command = conn.recv(1000)
        cmd = command.strip()
        print "Command",cmd
        if len(cmd.split(" "))==5:
            print "processing valid command"
            name,x,y,theta,image  = cmd.split(" ")
            self.spawn(float(x),float(y),float(theta),name,image)            
        conn.close()
        print "Handler finished"


if __name__=="__main__":
    rospy.init_node("positions_server")
    server = PositionsServer()
    rospy.spin()

