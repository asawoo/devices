
SSN: asawoo:c3po:gopigo
Pass: caff20024e55

Host: 192.168.10.1
Login SSH: pi
Pass: robots1234



# stop asawoo_gopigo service
sudo service asawoo_gopigo stop

sudo nano /etc/hosts
# map semantic-repos IP address

cd ~/asawoo

# clear runtime
sudo rm -r runtime

# load configuration
. /etc/default/asawoo

# set code location
## to host IP running code repo in a docker container
export code_location="http://192.168.10.2:3131/index.xml"
## or locally
# export code_location="file://bundles"

# create runtime environment
bin/dm conf/bundles.conf

# launch asawoo platform
cd runtime/appliance-gopigo/
java -jar -Dappliance.name=gopigo -Dasawoo.avatar.id=gopigo -Djava.util.logging.config.file=conf/logging.properties bin/felix.jar

# C3PO logs are located in
~/c3po/logs

# ASAWoO logs in
~/asawoo/logs
