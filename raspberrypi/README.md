# HOW-TO deploy ASAWoO on a RaspberryPi #

To install ASAWoO, you must proceed as follows.

## Compile ASAWoO ##

Since we use compiled JARs, go to `middleware/` and perform a `Maven clean install`

	$ cd ../middleware
	$ mvn clean install

## Make a deployable TAR archive ##

Create a tar.gz archive for the installation as follows:

    make tgz

## Install ##

Copy the archive on the Raspberry-pi using `ssh`

    scp asawoo-raspi.tar.gz YOUR_LOGIN@IP_ADDRESS_OF_YOUR_SERVER:

Connect to the Raspberry-pi using `ssh`, and extract the archive

    ssh YOUR_LOGIN@IP_ADDRESS_OF_YOUR_SERVER
    tar zxfv asawoo-raspi.tar.gz -C /home/pi/asawoo

Execute the script `setup/bin/install.sh` to install all the configuration files and
the C3PO middleware.
    $ cd setup
    $ sudo bin/install.sh

## Configure C3PO environment ##

If you own multiple ASAWoO Raspberries and want them to be unique, you will have to update their configuration.
The update script is in `setup/bin/update_net_conf.sh`.
Execute it with the following arguments: `<app_ID> <network_ID> <node_ID> [3G_gateway_SSID 3G_gateway_PSK]`

    $ cd setup
    $ sudo bin/update\_net\_conf.sh me17 asawoo RASP01

