#!/bin/bash
# Copyright (c) 2014-2017 IRISA, Université de Bretagne Sud, France.
#
# This file is part of the ASAWoO project.
#
# ASAWoO is free software; you can redistribute it and/or modify it under the
# terms of the CeCILL License. See CeCILL License for more details.
#
# ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY.
#
# Author: Nicolas Le Sommer
# Version: 1.0

#-------------------------------------------------------------------------------
# usage
#-------------------------------------------------------------------------------
#usage(){
#echo "install.sh <hostname>"
#echo "	- hostname : override default RaspberryPi hostname (i.e., raspberry)"

#}

#-------------------------------------------------------------------------------
# init
#-------------------------------------------------------------------------------

if [ "$(id -u)" != "0" ]; then
    echo "This script is used to install ASAWoO Access Points." 1>&2
    echo "It must be executed only after installing the Raspbian system." 1>&2
    echo "It must be executed as root." 1>&2
    exit 1
fi

# check if hostname/nodeId is given as argument
#if [ "$#" -ne 1 ]
#then
#  echo "Raspberry hostname / node_id must be passed as an argument"
#  usage
#  exit 1
#fi

# check if this script has been executed on this system
ASAWOO_ETC_DEFAULT_CONFIG_FILE="/etc/default/asawoo"

if [ -f $ASAWOO_ETC_DEFAULT_CONFIG_FILE ]; then
    echo "ASAWoO is already installed on this system!!!" 1>&2
    exit 1
fi

#-------------------------------------------------------------------------------
# Install missing debian packages and remove unnecessary ones
#-------------------------------------------------------------------------------
apt-get purge dhcpcd5 avahi-daemon samba-common
apt-get autoremove --purge
apt-get update
apt-get dist-upgrade
apt-get install hostapd wpasupplicant isc-dhcp-server openjdk-8-jre-headless gpsd python-gps firmware-realtek firmware-linux firmware-linux-nonfree firmware-atheros ntpdate rpi-update vim fping gettext

#-------------------------------------------------------------------------------
# Perform an update of clock and the firmware
#-------------------------------------------------------------------------------
/etc/init.d/ntp stop
ntpdate ntp1.univ-ubs.fr
rpi-update

#-------------------------------------------------------------------------------
# Network configuration
#-------------------------------------------------------------------------------

# wifi interfaces
wifi_interfaces=$(iw dev | grep Interface | sed 's/.*Interface\ \(.*\)/\1/g')
# find which interface is the USB one (i.e., the RT2870/RT3070 Wireless
# Adapter), and which one is installed on the Raspberry-pi board (the Broadcom
# interface).
for wi in $wifi_interfaces; do
    type=$(cat "/sys/class/net/$wi/device/modalias" | cut -d ":" -f 1)
    if [ "$type" == "sdio" ]; then
        wlan0_mac_addr=$(cat "/sys/class/net/$wi/address")
    else if [ "$type" == "usb" ]; then
             wlan1_mac_addr=$(cat "/sys/class/net/$wi/address")
         fi
    fi
done
# set the wireless interface names in /etc/systemd/network
# in the rest of the configuration wlan0 is considered as the Wi-Fi device
# installed on the Raspberry-pi board, and wlan1 the USB one

# wlan0
cat <<EOF > /etc/systemd/network/25-wlan0.link
[Match]
MACAddress=$wlan0_mac_addr
[Link]
Name=wlan0
EOF

# wlan1
cat <<EOF > /etc/systemd/network/25-wlan1.link
[Match]
MACAddress=$wlan1_mac_addr
[Link]
Name=wlan1
EOF

#-------------------------------------------------------------------------------
# Copy of /etc configuration files
#-------------------------------------------------------------------------------
cp -r etc/* /etc
cp -r usr/* /usr
# rm -rf etc

#-------------------------------------------------------------------------------
# Install the ASAWoO init.d script
#-------------------------------------------------------------------------------
systemctl enable /usr/local/lib/asawoo_net.service
systemctl enable /usr/local/lib/asawoo_raspi.service

# desactivate hostapd and isc-dhcp-server init.d scripts
systemctl disable hostapd.service
systemctl disable isc-dhcp-server.service
#systemctl disable gpsd.service

#-------------------------------------------------------------------------------
# Reboot
#-------------------------------------------------------------------------------
reboot
