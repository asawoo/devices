#!/bin/bash
#-------------------------------------------------------------------------------
# French ANR ASAWoO Project
#
# Deployment of OSGi platforms
#
# author: Lionel Touseau
# CASA research group, IRISA Laboratory, Université de Bretagne Sud
# version : 0.1.0
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Read configuration file
#-------------------------------------------------------------------------------
config_file=/etc/default/asawoo
if [ -f $config_file ]; then
    . $config_file
else
    echo "The configuration file $config_file does not exist." 1>&2
    exit 1
fi

#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------

echo "Stopping ASAWoO"
service asawoo_raspi stop

echo "Deleting ASAWoO TMP directory"
rm -rf /home/pi/asawoo/tmp-deploy-dir

echo "Deleting ASAWoO logs"
rm -rf $ASAWOO_LOG_DIR

echo "Deleting C3PO files"
rm -rf $C3PO_ROOT_DIR

echo "Data has been cleared, ASAWoO can be restarted."
echo "    sudo service asawoo_raspi start"
