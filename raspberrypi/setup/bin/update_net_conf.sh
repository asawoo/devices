#!/bin/bash
# Copyright (c) 2014-2017 IRISA, Université de Bretagne Sud, France.
#
# This file is part of the ASAWoO project.
#
# ASAWoO is free software; you can redistribute it and/or modify it under the
# terms of the CeCILL License. See CeCILL License for more details.
#
# ASAWoO is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY.
#
# Author: Nicolas Le Sommer
# Version: 1.0

RPI_ARCH="arm"
arch=$(uname -m | sed 's/\([a-z]\{3\}\).*/\1/g')

#-------------------------------------------------------------------------------
# usage
#-------------------------------------------------------------------------------
usage(){
    echo "Prepares the system to host a hub dedicated to an application"
    echo "Usage: $0 <app_ID> <network_ID> <node_ID> [3G_gateway_SSID 3G_gateway_PSK]"
    echo "Example: $0 asa asawoonet RASP01"
    echo "Example: $0 me17 asawoo RASP02 Domino-D762 33846567"
}

if [[ ($# -ne 3 && $# -ne 5) ]] ; then
	usage
	exit 1
fi

appid=$1
netid=$2
nodeid=$3

gwssid=$4
gwpsk=$5

echo "Updating RaspberryPi" 
echo "  appID  = $appid"
echo "  networkID   = $netid"
echo "  nodeID = $nodeid"
if [ "$gwssid" != "" ]; then
    echo " gw ssid = $gwssid"
    echo " gw psk  = $gwpsk"
fi

#-------------------------------------------------------------------------------
# Stop services
#-------------------------------------------------------------------------------

service asawoo_raspi stop
service asawoo_net stop

if [ $arch == $RPI_ARCH ]; then

    #-------------------------------------------------------------------------------
    # Set NodeID as HOSTNAME
    #-------------------------------------------------------------------------------
    oldhostname=$(hostname)
    
    echo $nodeid > /etc/hostname
    hostname $nodeid

    # in /etc/hosts replace the oldhostname by the new
    sed -ie "s/${oldhostname}/${nodeid}/g" /etc/hosts

    #/etc/init.d/hostname.sh start

    #-------------------------------------------------------------------------------
    # Read configuration file
    #-------------------------------------------------------------------------------
    config_file=/etc/default/asawoo

    if [ -f $config_file ]; then
	. $config_file
    else
	echo "The configuration file $config_file does not exist." 1>&2
	exit 1
    fi

    #-------------------------------------------------------------------------------
    # Updates C3PO environment
    #-------------------------------------------------------------------------------

    # conf/c3pocfg.json
    cat $C3PO_CONFIG_DIR/c3pocfg.json | tr '\n' '\r' | sed -e "s/\"c3po-app-name-prefix\"\([^\}]*\"value\":[ \t]*\)\"[^\"]*\"/\"c3po-app-name-prefix\"\1\"$appid\"/" -e  "s/\"netman-network-id\"\([^\}]*\"value\":[ \t]*\)\"[^\"]*\"/\"netman-network-id\"\1\"$netid\"/"  -e  "s/\"network-node-id\"\([^\}]*\"value\":[ \t]*\)\"[^\"]*\"/\"network-node-id\"\1\"$nodeid\"/" | tr '\r' '\n' > /tmp/c3pocfg.json
    \mv -f /tmp/c3pocfg.json	$C3PO_CONFIG_DIR/c3pocfg.json						       

    # /etc/hostapd/hostapd.conf
    sed -i -e "s/^ssid=.*$/ssid=$appid:$netid:$nodeid/" /etc/hostapd/hostapd.conf

    #-------------------------------------------------------------------------------
    # 3G/4G WPA_SUPPLICANT config
    #-------------------------------------------------------------------------------

    # /etc/wpa_supplicant/c3po.conf
    if [ "$gwssid" != "" ] ; then
	echo -e  "network={\n  ssid=\"$gwssid\"\n  psk=\"$gwpsk\"\n}" > /etc/wpa_supplicant/asawoo.conf
    fi

fi

#-------------------------------------------------------------------------------
# Restart services
#-------------------------------------------------------------------------------

if [ $arch == $RPI_ARCH ]; then
    echo "The infostation has been prepared, please reboot it, or turn it off..."    
else
    service asawoo_net start
    service asawoo_raspi start
fi


