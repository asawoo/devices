#!/bin/python
# This script is used to shutdown a Raspberry-pi when a button is pushed
# The button is connected to pin number 18, and to a ground pin.
# author: Nicolas Le Sommer

import RPi.GPIO as GPIO
import time
import os

# Use the Broadcom SOC pin numbers
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN, pull_up_down = GPIO.PUD_UP)

# The function that will be called when the button is pressed
def shutdown(channel):
    #os.system("sudo poweroff")
    os.system("sudo shutdown -h -P now")

# Detect when the button is pressed, and call the shutdown function
GPIO.add_event_detect(18, GPIO.FALLING, callback = shutdown, bouncetime = 2000)

# Now wait!
while True:
    time.sleep(1)
