import os
from gps import *
from time import *
import time
import threading
import math

#os.system('clear') #clear the terminal (optional)

if __name__ == '__main__':
    gpsd = gps(mode=WATCH_ENABLE) #starting the stream of info
    gpsd.next()
    while str(gpsd.fix.time) == "nan" :
        gpsd.next()
        time.sleep(5) 
        
#    print
#    print ' GPS reading'
#    print '----------------------------------------'
#    print 'latitude    ' , gpsd.fix.latitude
#    print 'longitude   ' , gpsd.fix.longitude
#    print 'time utc    ' , gpsd.utc,' + ', gpsd.fix.time
#    print 'altitude (m)' , gpsd.fix.altitude
#    print 'eps         ' , gpsd.fix.eps
#    print 'epx         ' , gpsd.fix.epx
#    print 'epv         ' , gpsd.fix.epv
#    print 'ept         ' , gpsd.fix.ept
#    print 'speed (m/s) ' , gpsd.fix.speed
#    print 'climb       ' , gpsd.fix.climb
#    print 'track       ' , gpsd.fix.track
#    print 'mode        ' , gpsd.fix.mode
#    print
#    print 'sats        ' , gpsd.satellites

    tsplit=gpsd.fix.time.split('T')
    date=tsplit[0]
    time=tsplit[1].split('.')[0]

#    cmd='timedatectl set-time "'+date+' '+time+'"'
    cmd='date -s "'+date+' '+time+'"'
    os.system('sudo '+cmd)
