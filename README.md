# ASAWoO-compliant devices

This directory contains configuration files and
Docker(<https://www.docker.com/)> images for ASAWoO-compliant devices.


## Raspberry-based devices

We provide configuration files for:
  * GoPiGo (<https://www.dexterindustries.com/gopigo3/>)
  * Raspberry equipped with the Raspbian operating system

## ROS-based devices

We provide Docker images
  * to simulate a Husky unmanned ground vehicle
    (<https://www.clearpathrobotics.com/husky-unmanned-ground-vehicle-robot/>)
  * to run Turtlesim (<http://wiki.ros.org/turtlesim>)
